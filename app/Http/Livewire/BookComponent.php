<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\Genre;
use Livewire\Component;
use App\Traits\BookService;

class BookComponent extends Component
{
    use BookService;

    public $showCreate = false;
    public $showConfirmation = false;

    public $state = [
        'title' => '',
        'author' => '',
        'link' => '',
        'short_description' => '',
        'cover' => ''
    ];

    public $editId;
    public $bookToDelete;

    public $selectedGenres = [];

    protected $rules = [
        'state.title' => 'required',
        'state.author' => 'required',
        'state.link' => 'required',
        'state.short_description' => 'required',
        'selectedGenres' => 'required'
    ];

    /* protected $queryString = [
        'showCreate' => ['except'=>false]
    ]; */

    public function render()
    {
        return view('livewire.book-component')->with([
            'books' => Book::with([
                'genres',
                'likes',
                'user_like',
                'dislikes',
                'user_dislike',
                'user_favorite',
                'user'
            ])->get(),
            'genres' => Genre::all(),
        ]);
    }

    public function showCreateForm()
    {
        $this->showCreate = !$this->showCreate;
    }

    public function save()
    {
        $this->validate();
        //dd($this->state);
        if ($this->editId) {
            $book = Book::find($this->editId);
            $book->update($this->state);
        } else {
            $book = Book::create(array_merge(['user_id' => auth()->id()], $this->state));
        }

        $book->genres()->sync($this->selectedGenres);

        $this->reset();
    }

    public function cancel()
    {
        $this->reset();
    }

    public function editBook(Book $book)
    {
        $book->load('genres');

        $this->editId = $book->id;
        $this->showCreate = true;

        $this->state['title'] = $book->title;
        $this->state['author'] = $book->author;
        $this->state['short_description'] = $book->short_description;
        $this->state['link'] = $book->link;

        foreach ($book->genres as $genre) {
            $this->selectedGenres[] = $genre->id;
        }
    }

    public function confirmDeleteBook(Book $book)
    {
        $this->bookToDelete = $book;
        $this->showConfirmation = true;
    }

    public function cancelDelete()
    {
        $this->reset();
    }

    public function deleteBook()
    {
        $this->bookToDelete->delete();

        $this->reset();
    }

    public function selectGenre(int $genre_id)
    {
        if (($key = array_search($genre_id, $this->selectedGenres)) !== false) {
            unset($this->selectedGenres[$key]);
        } else {
            $this->selectedGenres[] = $genre_id;
        }
    }
}
