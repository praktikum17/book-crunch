<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\User;
use App\Models\Genre;
use App\Traits\BookService;
use Livewire\Component;

class DashboardComponent extends Component
{
    use BookService;

    public function render()
    {
        return view('dashboard')->with([
            'genreCount' => Genre::count(),
            'bookCount' => Book::count(),
            'userCount' => User::count(),
            'recentBooks' => Book::orderBy('created_at', 'desc')->take(1)->with([
                'genres',
                'likes',
                'user_like',
                'dislikes',
                'user_dislike',
                'user_favorite',
                'user'
            ])->get(),
            'popularBooks' => Book::take(1)->with([
                'genres',
                'likes',
                'user_like',
                'dislikes',
                'user_dislike',
                'user_favorite',
                'user'
            ])->withCount('likes')->orderBy('likes_count', 'desc')->get()
        ]);
    }
}
