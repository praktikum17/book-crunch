<?php

namespace App\Http\Livewire;

use App\Models\Favorite;
use Livewire\Component;

class FavoriteComponent extends Component
{
    public function render()
    {
        return view('livewire.favorite-component')->with([
            'favorites'=>Favorite::with('book')->where('user_id',auth()->id())->get()
        ]);
    }

    public function markAsRead(Favorite $favorite)
    {
        $favorite->read = !$favorite->read;
        $favorite->save();
    }

    public function deleteFavorite(Favorite $favorite)
    {
        $favorite->delete();
    }
}
