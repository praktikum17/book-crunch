<?php

namespace App\Http\Livewire;

use App\Models\Book;
use App\Models\Genre;
use Livewire\Component;
use App\Traits\BookService;

class GenreComponent extends Component
{

    use BookService;

    public $showCreate = false;

    public $name, $genreId, $selectedGenre;

    public $genre, $editId;

    protected $queryString = [
        'genreId'
    ];

    public function render()
    {
        if ($this->genreId) {
            $this->selectedGenre = Genre::with('books')->find($this->genreId);
        }
        return view('livewire.genre-component')->with([
            'genres' => Genre::withCount('books')->orderBy('name')->get(),
        ]);
    }

    public function showCreateForm()
    {
        $this->showCreate = !$this->showCreate;
    }

    public function save()
    {
        if ($this->editId) {
            $genre = Genre::find($this->editId);
            $genre->name = $this->name;
            $genre->save();
        } else {
            Genre::create([
                'name' => $this->name
            ]);
        }


        $this->reset();
    }

    public function cancel()
    {
        $this->reset();
    }

    public function editGenre(Genre $genre)
    {
        $this->showCreate = true;
        $this->editId = $genre->id;
        $this->name = $genre->name;
    }

    public function deleteGenre(Genre $genre)
    {
        $genre->delete();
    }

    public function editBook(Book $book)
    {
        $book->load('genres');

        $this->editId = $book->id;
        $this->showCreate = true;

        $this->state['title'] = $book->title;
        $this->state['author'] = $book->author;
        $this->state['short_description'] = $book->short_description;
        $this->state['link'] = $book->link;

        foreach ($book->genres as $genre) {
            $this->selectedGenres[] = $genre->id;
        }
    }
}
