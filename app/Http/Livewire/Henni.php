<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Henni extends Component
{
    public function render()
    {
        return view('livewire.henni');
    }
}
