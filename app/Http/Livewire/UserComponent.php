<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class UserComponent extends Component
{
    public function render()
    {
        return view('livewire.user-component')->with([
            'users' => User::withCount('books')->get()
        ]);
    }
}
