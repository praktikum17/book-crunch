<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'author',
        'link',
        'short_description',
        'cover',
        'user_id'
    ];

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function user_like()
    {
        return $this->hasOne(Like::class)->where('user_id',auth()->id());
    }

    public function dislikes()
    {
        return $this->hasMany(Dislike::class);
    }

    public function user_dislike()
    {
        return $this->hasOne(Dislike::class)->where('user_id',auth()->id());
    }

    public function user_favorite()
    {
        return $this->hasOne(Favorite::class)->where('user_id',auth()->id());
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
    }
}
