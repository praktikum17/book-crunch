<?php

namespace App\Traits;

trait BookService
{
    public function likeBook(int $book_id)
    {
        $dislike = \App\Models\Dislike::where('book_id', $book_id)->where('user_id', auth()->id())->first();
        if ($dislike) {
            $dislike->delete();
        }
        $like = \App\Models\Like::where('book_id', $book_id)->where('user_id', auth()->id())->first();
        if (!$like) {
            \App\Models\Like::create([
                'user_id' => auth()->id(),
                'book_id' => $book_id
            ]);
        }
    }

    public function dislikeBook(int $book_id)
    {
        $like = \App\Models\Like::where('book_id', $book_id)->where('user_id', auth()->id())->first();
        if ($like) {
            $like->delete();
        }
        $dislike = \App\Models\Dislike::where('book_id', $book_id)->where('user_id', auth()->id())->first();
        if (!$dislike) {
            \App\Models\Dislike::create([
                'user_id' => auth()->id(),
                'book_id' => $book_id
            ]);
        }
    }

    public function favoriteBook(int $book_id)
    {
        $favorite = \App\Models\Favorite::where('book_id', $book_id)->where('user_id', auth()->id())->first();
        if ($favorite) {
            $favorite->delete();
        } else {
            \App\Models\Favorite::create([
                'user_id' => auth()->id(),
                'book_id' => $book_id
            ]);
        }
    }
}
