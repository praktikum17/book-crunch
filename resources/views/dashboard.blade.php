<div>
    <x-slot name="header">
        <h2 class="text-2xl font-semibold leading-tight text-gray-800">
            {{ __('♡Home♡') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div class="overflow-hidden bg-white shadow-xl sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 sm:px-20">
                    <div>
                        <x-jet-application-logo class="block w-auto h-12" />
                    </div>

                    <div class="mt-8 text-2xl">
                        Welcome to Book Crunch!<br />
                        by Henriette Krzewitzki
                    </div>

                    <div class="mt-6 text-gray-500">
                        Do you enjoy reading? If yes, welcome! This is the perfect website for bookworms.(^◡^ ) If no, welcome!
                        I hope you find your first books. ᕙ(`▿´)ᕗ
                        Book Crunch helps you find the perfect books according to your liking!
                        You can search for books, explore, upload new books etc. I hope you love it!
                        (ɔ◔‿◔)ɔ ♥ ~Spread positivity~ -`ღ´-A reader lives a thousand lives before he dies-`ღ´- [George R.R. Martin] !GOOD VIBES ONLY!(ง'̀-'́)ง
                        <br/>
                        To Arnisa: Diese Deutschland💕
                    </div>

                    <div class="flex mt-6 space-x-6">
                        <span>Genre: {{ $genreCount }}</span>
                        <span>Bücher: {{ $bookCount }}</span>
                        <span>Nutzer: {{ $userCount }}</span>
                    </div>
                    <div class="mt-6">
                        <h1 class="mb-2 text-xl">Most Recent</h1>
                        <hr>
                        @foreach ($recentBooks as $book)
                            <div class="flex w-full mt-5">
                                <div class="flex w-full">
                                    <div class="flex pr-1">
                                        @if (!$book->user_favorite)
                                            <x-heroicon-o-bookmark wire:click="favoriteBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @else
                                            <x-heroicon-s-bookmark wire:click="favoriteBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @endif
                                    </div>
                                    <h2 class="text-lg">{{ $book->title }}</h2>
                                    <div class="flex">
                                        @if (!$book->user_like)
                                            <x-heroicon-o-thumb-up wire:click="likeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @else
                                            <x-heroicon-s-thumb-up wire:click="likeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @endif
                                        <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->likes->count() }})</span>
                                    </div>
                                    <div class="flex">
                                        @if (!$book->user_dislike)
                                            <x-heroicon-o-thumb-down wire:click="dislikeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @else
                                            <x-heroicon-s-thumb-down wire:click="dislikeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @endif
                                        <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->dislikes->count() }})</span>
                                    </div>
                                </div>
                            </div>
                            <div class="w-full px-12 pb-2">
                                by {{ $book->author }} <span class="text-xs">({{ $book->user->name }})</span>
                                <hr class="my-2">
                                <p>
                                    {{ $book->short_description }}
                                </p>
                                <p class="py-2">
                                    <a class="px-2 py-1 text-xs text-white bg-blue-700 rounded hover:bg-blue-500" target="_blank" class="hover:underline" href="{{ $book->link }}">Buy Online</a>
                                </p>
                            </div>
                            <div class="flex w-full px-12 pb-10 space-x-2">
                                @foreach ($book->genres as $genre)
                                    <a href="{{ route('GENRE', ['genreId' => $genre->id]) }}" class="px-2 py-1 text-xs text-white bg-pink-600 rounded-lg hover:bg-pink-300 hover:text-pink-600">{{ $genre->name }}</a>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                    <div class="mt-6">
                        <h1 class="mb-2 text-xl">Most Popular</h1>
                        <hr>
                        @foreach ($popularBooks as $book)
                            <div class="flex w-full mt-5">
                                <div class="flex w-full">
                                    <div class="flex pr-1">
                                        @if (!$book->user_favorite)
                                            <x-heroicon-o-bookmark wire:click="favoriteBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @else
                                            <x-heroicon-s-bookmark wire:click="favoriteBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @endif
                                    </div>
                                    <h2 class="text-lg">{{ $book->title }}</h2>
                                    <div class="flex">
                                        @if (!$book->user_like)
                                            <x-heroicon-o-thumb-up wire:click="likeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @else
                                            <x-heroicon-s-thumb-up wire:click="likeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @endif
                                        <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->likes->count() }})</span>
                                    </div>
                                    <div class="flex">
                                        @if (!$book->user_dislike)
                                            <x-heroicon-o-thumb-down wire:click="dislikeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @else
                                            <x-heroicon-s-thumb-down wire:click="dislikeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                        @endif
                                        <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->dislikes->count() }})</span>
                                    </div>
                                </div>
                            </div>
                            <div class="w-full px-12 pb-2">
                                by {{ $book->author }} <span class="text-xs">({{ $book->user->name }})</span>
                                <hr class="my-2">
                                <p>
                                    {{ $book->short_description }}
                                </p>
                                <p class="py-2">
                                    <a class="px-2 py-1 text-xs text-white bg-blue-700 rounded hover:bg-blue-500" target="_blank" class="hover:underline" href="{{ $book->link }}">Buy Online</a>
                                </p>
                            </div>
                            <div class="flex w-full px-12 pb-10 space-x-2">
                                @foreach ($book->genres as $genre)
                                    <a href="{{ route('GENRE', ['genreId' => $genre->id]) }}" class="px-2 py-1 text-xs text-white bg-pink-600 rounded-lg hover:bg-pink-300 hover:text-pink-600">{{ $genre->name }}</a>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
