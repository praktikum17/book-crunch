<div>
    <x-slot name="header">
        <h2 class="text-2xl font-semibold leading-tight text-gray-800">
            {{ __('Book list(◠‿◠) ') }}
        </h2>
    </x-slot>
    <div class="p-5 mx-auto mt-10 bg-white rounded-lg shadow-lg max-w-7xl">
        <div class="flex justify-between w-full py-4">
            <h1 class="text-2xl">All books ٩(˘◡˘)۶ </h1>

            <button class="px-2 py-1 text-pink-400 bg-pink-200 rounded hover:bg-pink-400 hover:text-white"
                wire:click="showCreateForm">Neuer Eintrag</button>
        </div>
        @if ($showCreate)
            <div class="w-full py-2">
                <x-jet-label for="title" value="{{ __('Titel') }}" />
                <x-jet-input id="title" class="block w-full mt-1" type="text" name="title" wire:model="state.title"
                    required />
                <x-jet-label class="mt-2" for="author" value="{{ __('Autor') }}" />
                <x-jet-input id="author" class="block w-full mt-1" type="text" name="author" wire:model="state.author"
                    required />
                <x-jet-label class="mt-2" for="link" value="{{ __('Link (Amazon etc.)') }}" />
                <x-jet-input id="link" class="block w-full mt-1" type="text" name="link" wire:model="state.link"
                    required />
                <x-jet-label class="mt-2" for="short_description" value="{{ __('Kurzbeschreibung') }}" />
                <textarea id="short_description" name="short_description" wire:model="state.short_description"
                    class="w-full border border-gray-300"></textarea>
                <x-jet-label class="mt-2" for="genres" value="{{ __('Genres') }}" />
                <div class="flex flex-wrap w-full">
                    @foreach ($genres as $genre)
                        <button type="button" wire:click="selectGenre({{ $genre->id }})"
                            @class([
                                'flex px-2 py-1 mb-2 mr-2 text-xs rounded text-white',
                                'bg-pink-600 hover:bg-pink-800' => in_array($genre->id, $selectedGenres),
                                'bg-blue-600 hover:bg-blue-800' => !in_array($genre->id, $selectedGenres)
                            ])>
                            {{ $genre->name }}
                        </button>
                    @endforeach
                </div>
                <div class="flex justify-between w-full pt-5">
                    <button class="px-2 py-1 text-white bg-red-800 rounded hover:bg-red-400"
                        wire:click="cancel">Abbrechen</button>
                    <button class="px-2 py-1 text-pink-400 bg-pink-200 rounded hover:bg-pink-400 hover:text-white"
                        wire:click="save">Speichern</button>
                </div>
            </div>
        @else
            @foreach ($books as $book)
                <div class="flex w-full">
                    <div class="flex w-full">
                        <div class="flex pr-1">
                            @if (!$book->user_favorite)
                                <x-heroicon-o-bookmark wire:click="favoriteBook({{ $book->id }})"
                                    class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                            @else
                                <x-heroicon-s-bookmark wire:click="favoriteBook({{ $book->id }})"
                                    class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                            @endif
                        </div>
                        <h2 class="text-lg">{{ $book->title }}</h2>
                        <div class="flex">
                            @if (!$book->user_like)
                                <x-heroicon-o-thumb-up wire:click="likeBook({{ $book->id }})"
                                    class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                            @else
                                <x-heroicon-s-thumb-up wire:click="likeBook({{ $book->id }})"
                                    class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                            @endif
                            <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->likes->count() }})</span>
                        </div>
                        <div class="flex">
                            @if (!$book->user_dislike)
                                <x-heroicon-o-thumb-down wire:click="dislikeBook({{ $book->id }})"
                                    class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                            @else
                                <x-heroicon-s-thumb-down wire:click="dislikeBook({{ $book->id }})"
                                    class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                            @endif
                            <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->dislikes->count() }})</span>
                        </div>
                        <div class="flex pl-2 space-x-2">
                            @if ($book->user_id === auth()->id())
                                <x-heroicon-o-pencil wire:click.prevent="editBook({{ $book->id }})"
                                    class="w-5 h-5 ml-5 text-pink-500 cursor-pointer" />
                                <x-heroicon-o-trash wire:click.prevent="confirmDeleteBook({{ $book->id }})"
                                    class="w-5 h-5 ml-5 text-pink-500 cursor-pointer" />
                            @endif
                        </div>
                    </div>
                </div>
                <div class="w-full px-12 pb-2">
                    by {{ $book->author }} <span class="text-xs">({{ $book->user->name }})</span>
                    <hr class="my-2">
                    <p>
                        {{ $book->short_description }}
                    </p>
                    <p class="py-2">
                        <a class="px-2 py-1 text-xs text-white bg-blue-700 rounded hover:bg-blue-500" target="_blank"
                            class="hover:underline" href="{{ $book->link }}">Buy Online</a>
                    </p>
                </div>
                <div class="flex w-full px-12 pb-10 space-x-2">
                    @foreach ($book->genres as $genre)
                        <a href="{{ route('GENRE', ['genreId' => $genre->id]) }}"
                            class="px-2 py-1 text-xs text-white bg-pink-600 rounded-lg hover:bg-pink-300 hover:text-pink-600">{{ $genre->name }}</a>
                    @endforeach
                </div>
            @endforeach
        @endif
    </div>
    <x-jet-dialog-modal wire:model='showConfirmation'>
        <x-slot name="title">Delete Book?</x-slot>
        <x-slot name="content">
            The book {{ $bookToDelete->title ?? '' }} will be deleted.
        </x-slot>
        <x-slot name="footer">
            <div class="flex space-x-4">
                <x-jet-button type="button" wire:click="cancelDelete">Abbrechen</x-jet-button>
                <x-jet-danger-button type="button" wire:click="deleteBook">Löschen</x-jet-danger-button>
            </div>
        </x-slot>
    </x-jet-dialog-modal>
</div>
