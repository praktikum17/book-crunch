<div>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Meine Favoriten') }}
        </h2>
    </x-slot>
    <div class="p-5 mx-auto mt-10 bg-white rounded-lg shadow-lg max-w-7xl">

        <div class="flex flex-col w-full space-y-2">
            @foreach ($favorites as $favorite)
            <div class="flex space-x-2">
                @if($favorite->read)
                <x-heroicon-o-x wire:click="markAsRead({{$favorite->id}})" class="w-4 h-4 text-red-600 cursor-pointer"></x-heroicon-o-pencil>
                @else
                <x-heroicon-o-check wire:click="markAsRead({{$favorite->id}})" class="w-4 h-4 text-red-600 cursor-pointer"></x-heroicon-o-pencil>
                @endif

                <x-heroicon-o-trash wire:click="deleteFavorite({{$favorite->id}})" class="w-4 h-4 text-pink-600 cursor-pointer"></x-heroicon-o-pencil>
                <a @class([
                    'line-through' => $favorite->read
                ]) href="{{route('BOOK',['bookId'=>$favorite->book->id])}}">{{$favorite->book->title}}</a>
            </div>

            @endforeach
        </div>
    </div>
</div>
