<div>
    <x-slot name="header">
        <h2 class="text-2xl font-semibold leading-tight text-gray-800">
            @if($selectedGenre)
            {{$selectedGenre->name}}
            @else
                {{ __('Genre list (❛‿❛)') }}
            @endif
        </h2>
    </x-slot>
    <div class="p-5 mx-auto mt-10 bg-white rounded-lg shadow-lg max-w-7xl">
        @if ($selectedGenre)
            <div class="pt-5">
                @forelse ($selectedGenre->books as $book)
                    <div class="flex w-full">
                        <div class="flex w-full">
                            <div class="flex pr-1">
                                @if (!$book->user_favorite)
                                    <x-heroicon-o-bookmark wire:click="favoriteBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                @else
                                    <x-heroicon-s-bookmark wire:click="favoriteBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                @endif
                            </div>
                            <h2 class="text-lg">{{ $book->title }}</h2>
                            <div class="flex">
                                @if (!$book->user_like)
                                    <x-heroicon-o-thumb-up wire:click="likeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                @else
                                    <x-heroicon-s-thumb-up wire:click="likeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                @endif
                                <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->likes->count() }})</span>
                            </div>
                            <div class="flex">
                                @if (!$book->user_dislike)
                                    <x-heroicon-o-thumb-down wire:click="dislikeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                @else
                                    <x-heroicon-s-thumb-down wire:click="dislikeBook({{ $book->id }})" class="w-6 h-6 ml-5 text-pink-500 cursor-pointer" />
                                @endif
                                <span class="mt-2 ml-2 text-xs text-pink-600">({{ $book->dislikes->count() }})</span>
                            </div>
                        </div>
                    </div>
                    <div class="w-full px-12 pb-2">
                        by {{ $book->author }} <span class="text-xs">({{ $book->user->name }})</span>
                        <hr class="my-2">
                        <p>
                            {{ $book->short_description }}
                        </p>
                        <p class="py-2">
                            <a class="px-2 py-1 text-xs text-white bg-blue-700 rounded hover:bg-blue-500" target="_blank" class="hover:underline" href="{{ $book->link }}">Buy Online</a>
                        </p>
                    </div>
                    <div class="flex w-full px-12 pb-10 space-x-2">
                        @foreach ($book->genres as $genre)
                            <a href="{{ route('GENRE', ['genreId' => $genre->id]) }}" class="px-2 py-1 text-xs text-white bg-pink-600 rounded-lg hover:bg-pink-300 hover:text-pink-600">{{ $genre->name }}</a>
                        @endforeach
                    </div>
                @empty
                    {{ __('Es sind noch keine Bücher vorhanden.') }}
                @endforelse
                <div class="w-full mt-10">
                    <a href="{{ route('GENRE') }}" class="px-2 py-1 mt-5 text-pink-400 bg-pink-200 rounded hover:bg-pink-400 hover:text-white">Zurück</a>

                </div>
            </div>
        @else
            <div class="flex justify-between w-full py-4">
                <h1 class="text-2xl">All genres (•◡•) /</h1>
                <button class="px-2 py-1 text-pink-400 bg-pink-200 rounded hover:bg-pink-400 hover:text-white" wire:click="showCreateForm">Neuer Eintrag</button>
            </div>
            @if ($showCreate)
                <div class="w-full py-2">
                    <x-jet-label for="name" value="{{ __('Name') }}" />
                    <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" wire:model="name" required />
                    <div class="flex justify-between w-full pt-5">
                        <button class="px-2 py-1 text-white bg-red-800 rounded hover:bg-red-400" wire:click="cancel">Abbrechen</button>
                        <button class="px-2 py-1 text-pink-400 bg-pink-200 rounded hover:bg-pink-400 hover:text-white" wire:click="save">Speichern</button>
                    </div>
                </div>
            @else
                <div class="flex flex-col w-full space-y-2">
                    @foreach ($genres as $genre)
                        <div class="flex space-x-2">
                            <x-heroicon-o-trash wire:click="deleteGenre({{ $genre->id }})" class="w-4 h-4 text-red-600 cursor-pointer">
                                </x-heroicon-o-pencil>
                                <x-heroicon-o-pencil wire:click="editGenre({{ $genre->id }})" class="w-4 h-4 text-pink-600 cursor-pointer"></x-heroicon-o-pencil>
                                <a class="hover:underline" href="{{ route('GENRE', ['genreId' => $genre->id]) }}">{{ $genre->name }}</a> ({{ $genre->books_count }})
                        </div>
                    @endforeach
                </div>

            @endif
        @endif
    </div>
</div>
