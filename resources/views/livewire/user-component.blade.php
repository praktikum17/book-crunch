<div>
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800">
            {{ __('Users') }}
        </h2>
    </x-slot>
    <div class="p-5 mx-auto mt-10 bg-white rounded-lg shadow-lg max-w-7xl">
        <div class="flex flex-wrap">
            @foreach ($users as $user)
                <div class="flex flex-col py-2 mr-5">
                    <img src="{{ $user->profile_photo_url }}" alt="{{ $user->name }}" class="object-cover w-20 h-20 rounded-full">
                    <h1 class="text-lg">{{ $user->name }}</h1>
                    <span class="text-sm">Since: {{ $user->created_at->format('d.m.Y') }}</span>
                    <span class="text-sm">Books: {{ $user->books_count }}</span>
                </div>
            @endforeach
        </div>
    </div>
</div>
