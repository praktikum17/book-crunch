<div class="p-6 bg-white border-b border-gray-200 sm:px-20">
    <div>
        <x-jet-application-logo class="block w-auto h-12" />
    </div>

    <div class="mt-8 text-2xl">
        Welcome to Book Crunch!<br/>
        by Henriette Krzewitzki
    </div>

    <div class="mt-6 text-gray-500">
        Laravel Jetstream provides a beautiful, robust starting point for your next Laravel application. Laravel is designed
        to help you build your application using a development environment that is simple, powerful, and enjoyable. We believe
        you should love expressing your creativity through programming, so we have spent time carefully crafting the Laravel
        ecosystem to be a breath of fresh air. We hope you love it.
    </div>

    <div class="flex mt-6 space-x-6">
        <span>Genre: {{$genreCount}}</span>
        <span>Bücher: {{$bookCount}}</span>
        <span>Nutzer: {{$userCount}}</span>
    </div>
</div>
