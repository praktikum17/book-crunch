<x-guest-layout>
    <div
        class="relative flex justify-center min-h-screen py-4 bg-gray-100 items-top dark:bg-gray-900 sm:items-center sm:pt-0">

        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div class="flex flex-col justify-center pt-8 sm:justify-start sm:pt-0">
                <x-jet-application-mark class="block w-auto h-20 text-pink-600" /> <br />
                <span class="text-lg">Book Crunch v1.0</span><br/>
                <span class="text-sm">By Henriette Krzewitzki</span>
            </div>
            @if (Route::has('login'))
            <div class="justify-between w-full">
                @auth
                    <a href="{{ url('/dashboard') }}"
                        class="text-sm text-gray-700 underline dark:text-gray-500">Dashboard</a>
                @else
                    <a href="{{ route('login') }}" class="text-sm text-gray-700 underline dark:text-gray-500">Log in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}"
                            class="ml-4 text-sm text-gray-700 underline dark:text-gray-500">Register</a>
                    @endif
                @endauth
            </div>
            @endif
        </div>
    </div>
</x-guest-layout>
