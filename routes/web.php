<?php

use App\Http\Livewire\Henni;
use App\Http\Livewire\BookComponent;
use App\Http\Livewire\GenreComponent;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\FavoriteComponent;
use App\Http\Livewire\DashboardComponent;
use App\Http\Livewire\UserComponent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', DashboardComponent::class)->name('dashboard');
    Route::get('henni', Henni::class)->name('HENNI');
    Route::get('genres', GenreComponent::class)->name('GENRE');
    Route::get('books', BookComponent::class)->name('BOOK');
    Route::get('favorites', FavoriteComponent::class)->name('FAVORITE');
    Route::get('users', UserComponent::class)->name('USER');
});
